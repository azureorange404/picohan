package com.azureorange.picohan;

import android.util.Log;

import java.util.ArrayList;

/** @noinspection SameReturnValue*/
public class PlayerSettings {

    public static boolean addedPlayer;
    public static String newPlayer;
    public static int playerCount = 0;
    public static final ArrayList<String> playerList = new ArrayList<>();

    // game settings
    public static final int minPlayers = 2;
    public static final int maxPlayers = 8;

    public PlayerSettings(String input) {

        if (playerCount < maxPlayers) { // make sure player limit is not exceeded
            // Add input to player_list

            playerList.add(input);
            newPlayer = input;

            Log.d("INDEX", "player-added: " + playerCount);

            // count + 1
            playerCount++;
        }
    }
    public static String getNewPlayer() {
        return newPlayer;
    }

    public static int getPlayerCount() {
        Log.d("INDEX", "getPlayerCount: " + playerCount);
        return playerCount;
    }

    public static void resetPlayerCount() {
        playerCount = 0;
    }

    public static int getMinPlayers() {
        return minPlayers;
    }

    public static ArrayList<String> getPlayerList() {
        return playerList;
    }

    public static boolean newPlayerAdded() {
        addedPlayer = playerCount <= maxPlayers;
        return addedPlayer;
    }

    public static void removePlayer(int index) {
        playerList.remove(index);
        if ( playerCount > 0) {
            playerCount--;
        }
    }

    public static void clear() {
        playerList.clear();
    }
}
