package com.azureorange.picohan;

import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/** @noinspection CommentedOutCode*/
public class generateCardDeck {

    public final List<String> cardTextDeck;
    public final List<String> cardTitleDeck;
    public final List<String> defaultList;
    public final List<String> virusList;
    public final List<String> gameList;
    public final List<String> cureList;

    public String statement;

    public final int maxTurns = 30;
    public int finalMaxTurns;
    public int virusCount = 0;

    public Boolean game = true;

    public generateCardDeck(List<String> defaultArray, List<String> virusArray, List<String> gameArray) {

        defaultList = new LinkedList<>(defaultArray);
        virusList = new LinkedList<>(virusArray);
        gameList = new LinkedList<>(gameArray);

        cureList = new ArrayList<>();

        cardTitleDeck = new ArrayList<>();
        cardTextDeck = new ArrayList<>();

        finalMaxTurns = maxTurns;

        for (int x = 1; x <= maxTurns; x++) {

            Random cureRand = new Random();
            int fiftyPercent = cureRand.nextInt(2) + 1;

            if (virusCount > 0 && x % 4 == 0 && fiftyPercent == 1) {
                // cure triggered
                setCure();
                x--; // do not use with maxTurns++
            } else { // trigger a card

                // every 5th turn triggers a special card
                if (x % 5 == 0) {
                    // 50:50 if it is a game or a virus
                    Random rand = new Random();
                    int i = rand.nextInt(1001); i++;

                    if (i % 2 == 0 && x <= (maxTurns - 5) && virusList.size() > 0) {
                        // virus triggered
                        setVirus();
                    } else if (!game && gameList.size() > 0) {
                        // game triggered
                        setGame();
                    } else {
                        // normal question triggered
                        setDefault();
                    }
                } else {
                    Random gameRand = new Random();
                    int twentyPercent = gameRand.nextInt(5) + 1;

                    if (twentyPercent == 3 && !game && gameList.size() > 0) {
                        // game triggered
                        setGame();
                    } else {
                        // normal question triggered
                        setDefault();
                    }
                }
            }
        }

        while (virusCount > 0) {
            setCure();
            // cardTextDeck.add("All remaining infected players were cured.");
        }
        cardTextDeck.add("");
        cardTitleDeck.add("Game Over");

        for (int i = 0; i < cardTextDeck.size(); i++) {
            Log.d("generatedCards", cardTextDeck.get(i));
        }
    }

    public String getDefault() {
        // pick random index
        int index = new Random().nextInt(defaultList.size());
        // set q to be the string from the array
        String q = defaultList.get(index);
        // remove the question
        defaultList.remove(index);

        return q;
    }

    public void setDefault() {
        statement = getDefault();
        cardTextDeck.add(new Question(statement).getQuestion());
        cardTitleDeck.add(new Question(statement).getTitle());
        game = false;
    }

    public String getVirus(int index) {
        // set q to be the string from the array
        String q = virusList.get(index);
        // remove the question
        virusList.remove(index);

        return q;
    }

    public void setVirus() {
        // pick random index from virusArray
        int index = new Random().nextInt(virusList.size());
        if (index % 2 != 0) {
            // make the index odd as viruses are odd numbers
            index--;
        }
        statement = getVirus(index);
        cureList.add(getCure(index));
        cardTextDeck.add(new Virus(statement).getVirus());
        cardTitleDeck.add("Virus");
        virusCount++;
        game = false;
    }

    public String getCure(int index) {
        // set q to be the string from the array
        String q = virusList.get(index);
        // remove the question
        virusList.remove(index);

        return q;
    }

    public void setCure() {
/*
        Random cureRand = new Random();
        int index = cureRand.nextInt(virusCount);
*/

        cardTextDeck.add(new Virus(cureList.get(0)).getVirus());
        cardTitleDeck.add("Cure");

        cureList.remove(0);
        virusCount--;
        // was maxTurns++;
        finalMaxTurns++;
    }

    private String getGame() {
        // pick random index
        int index = new Random().nextInt(gameList.size());
        // set q to be the string from the array
        String q = gameList.get(index);
        // remove the question
        gameList.remove(index);

        return q;
    }

    public void setGame() {
        statement = getGame();
        cardTextDeck.add(new Game(statement).getGame());
        cardTitleDeck.add("Game");
        game = true;
    }

    public List<String> getCardTitleList() {
        return cardTitleDeck;
    }

    public List<String> getCardTextList() {
        return cardTextDeck;
    }

    public int getMaxTurns() {
        return finalMaxTurns;
    }

}
