package com.azureorange.picohan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Virus {

    public final String question;
    public static String infectedPlayerName;
    public static String infected2PlayerName;
    final List<String> playerArray;
    public static final List<String> curePlayerList = new ArrayList<>();


    public Virus(String tempQuestion) {
        super();

        // get Players and Player count
        playerArray = new LinkedList<>(PlayerSettings.getPlayerList());
        int playerCount = PlayerSettings.getPlayerCount();

        Collections.shuffle(playerArray);

        // change [PRIMARY] to player name
        Pattern primaryPattern = Pattern.compile("\\[PRIMARY]");
        Matcher primaryMatcher = primaryPattern.matcher(tempQuestion);

        if (primaryMatcher.find()) {
            int index = new Random().nextInt(playerCount);
            // set persistent infectedPlayerName for the Cure later
            infectedPlayerName = playerArray.get(index);
            curePlayerList.add(infectedPlayerName);
            tempQuestion = tempQuestion.replace(primaryMatcher.group(), infectedPlayerName);
            playerArray.remove(index);
            playerCount = playerCount - 1;
        }

        // change [NAME] to player name
        Pattern namePattern = Pattern.compile("\\[NAME]");
        Matcher nameMatcher = namePattern.matcher(tempQuestion);

        if (nameMatcher.find()) {
            String playerToCure = curePlayerList.get(0);
            tempQuestion = tempQuestion.replace(nameMatcher.group(), playerToCure);
            curePlayerList.remove(0);
        }

        Pattern secondaryPattern = Pattern.compile("\\[SECONDARY]");
        Matcher secondaryMatcher = secondaryPattern.matcher(tempQuestion);

        if (secondaryMatcher.find()) {
            int index = new Random().nextInt(playerCount);
            infected2PlayerName = playerArray.get(index);
            curePlayerList.add(infected2PlayerName);
            tempQuestion = tempQuestion.replace(secondaryMatcher.group(), infected2PlayerName);
            playerArray.remove(index);
        }

        // change [NAME] to player name
        Pattern name2Pattern = Pattern.compile("\\[NAME2]");
        Matcher name2Matcher = name2Pattern.matcher(tempQuestion);

        if (name2Matcher.find()) {
            String playerToCure = curePlayerList.get(0);
            tempQuestion = tempQuestion.replace(name2Matcher.group(), playerToCure);
            curePlayerList.remove(0);
        }

        question = tempQuestion;
    }

    public String getVirus() {
        return question;
    }

    public static void clearCurePlayers() {
        curePlayerList.clear();
    }
}
