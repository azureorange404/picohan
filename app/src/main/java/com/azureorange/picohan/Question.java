package com.azureorange.picohan;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
//import java.util.Objects;
import java.util.Objects;
import java.util.Random;
//import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Question {

    public String title = "";
    public final String question;
    final List<String> playerArray;
    public static int playerCount;


    public Question(String tempQuestion) {
        super();

        // search for the title pattern and extract the title
        Pattern titlePattern = Pattern.compile("\\[\\[(.*?)]]");
        Matcher titleMatcher = titlePattern.matcher(tempQuestion);

        // remove the title from the question and assign the title and question variable
        // if group is 1 the regex is excluded, if it is 0, it is included
        if (titleMatcher.find()) {
//            tempQuestion = srcQuestion.replace("[[" + titleMatcher.group(1).trim() + "]]", "");
            tempQuestion = tempQuestion.replace(Objects.requireNonNull(titleMatcher.group(0)).trim(), "");
            title = Objects.requireNonNull(titleMatcher.group(1)).trim();
        }

        // search for the player pattern and extract the title

        playerArray = new LinkedList<>(PlayerSettings.getPlayerList());
        playerCount = PlayerSettings.getPlayerCount();

        Collections.shuffle(playerArray);

        tempQuestion = replacePlayerTag(tempQuestion, "\\[PRIMARY]");

        tempQuestion = replacePlayerTag(tempQuestion, "\\[SECONDARY]");

        question = tempQuestion;
    }

    public String replacePlayerTag(String searchString, String regex) {

        Pattern regexPattern = Pattern.compile(regex);
        Matcher regexMatcher = regexPattern.matcher(searchString);

        if (regexMatcher.find()) {
            int index = new Random().nextInt(playerCount);
            searchString = searchString.replace(regexMatcher.group(), playerArray.get(index));
            playerArray.remove(index);
            playerCount--;
        }

        return searchString;
    }

    public String getQuestion() {
        return question;
    }

    public String getTitle() {
        return title;
    }
}
