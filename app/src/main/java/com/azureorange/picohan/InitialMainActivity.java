package com.azureorange.picohan;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.google.android.flexbox.FlexboxLayout;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Objects;

/** @noinspection InstantiationOfUtilityClass*/
public class InitialMainActivity extends AppCompatActivity implements View.OnClickListener {

    static final ArrayList<Button> buttons = new ArrayList<>();

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // If the Android version is lower than Jellybean, use this call to hide
        // the status bar. https://developer.android.com/training/system-ui/status
//        if (Build.VERSION.SDK_INT > 16) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        }

        setContentView(R.layout.activity_main);

        PlayerSettings.resetPlayerCount();

        // Display drinking disclaimer message
        popupMessage("Disclaimer", "You should drink responsibly!");

        // lock orientation to portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Define TextInputEditText from activity_player.xml
        TextInputEditText player_input_field = findViewById(R.id.player_name_input_field);

        // Define plus button
        Button addPlayerButton = findViewById(R.id.add_player_button);

        // Define the layout containing player tags
        FlexboxLayout buttonContainer = findViewById(R.id.buttonContainer);

        // What happens when the plus button is clicked?
        addPlayerButton.setOnClickListener(
                view -> {
                    // get the text from the input field
                    String input = Objects.requireNonNull(player_input_field.getText()).toString();
                    new PlayerSettings(input);

                    if ( PlayerSettings.newPlayerAdded() ) { // make sure player limit is not exceeded

                        // set the TextView to display the player name
                        int index = PlayerSettings.getPlayerCount() - 1;
                        Log.d("INDEX", "set button ID: " + index);

                        Button button = new Button(this);
                        button.setText(PlayerSettings.getNewPlayer());
                        button.setMinimumHeight(0);
                        button.setMinimumWidth(0);
                        button.setMinHeight(0);
                        button.setMinWidth(0);
                        button.setId(index);
                        button.setOnClickListener(this);
                        buttons.add(button);
                        buttonContainer.addView(button);
                    }
                    player_input_field.getText().clear();
                    InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
        );

        // starting the game

        Button startButton = findViewById(R.id.start_button);

        startButton.setOnClickListener(
                view -> {
                    if (PlayerSettings.getPlayerCount() < PlayerSettings.getMinPlayers()) {
                        popupMessage("Player Count", "You need at least two players to play this game.");
                    } else {
                        switchActivities();
                    }
                }
        );

    }

    public void popupMessage(String msgTitle, String msgBody){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(msgBody);
//        alertDialogBuilder.setIcon(R.drawable.ic_no_internet);
        alertDialogBuilder.setTitle(msgTitle);
        alertDialogBuilder.setNegativeButton("ok", (dialogInterface, i) -> {
            Log.d("player_count","Ok btn pressed");
            // add these two lines, if you wish to close the app:
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // Method to switch Activity
    private void switchActivities() {
        Intent switchActivityIntent = new Intent(this, PicohanActivity.class);
        startActivity(switchActivityIntent);
    }

    @Override
    public void onClick(View button) {
        int buttonId = button.getId();
        Log.d("INDEX", "rm button ID: " + buttonId);
//        String ID = Integer.toString(buttonId);
        PlayerSettings.removePlayer(buttonId);
        buttons.get(buttonId).setVisibility(View.GONE);
        buttons.remove(buttonId);
        for (int i = 0; i < buttons.size(); i++) {
            buttons.get(i).setId(i);
        }
    }
}