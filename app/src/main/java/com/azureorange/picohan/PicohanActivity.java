package com.azureorange.picohan;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class PicohanActivity extends AppCompatActivity {

    List<String> questionArray;
    List<String> virusArray;
    List<String> gameArray;

    /** @noinspection ConstantConditions*/
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("Activity", "is created");

        // If the Android version is lower than Jellybean, use this call to hide
        // the status bar. https://developer.android.com/training/system-ui/status
//        if (Build.VERSION.SDK_INT > 16) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        }

        setContentView(R.layout.activity_picohan);

        // lock orientation to landscape
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // Set game mode and pick the corresponding question array
        String gameMode = "default";

        if (gameMode.equals("default")) {
            questionArray = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.default_questions)));
            virusArray = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.viruses)));
            gameArray = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.games)));
        }

        RelativeLayout touch = findViewById(R.id.touch);
        final TextView questionText = findViewById(R.id.question);
        final TextView questionTitle = findViewById(R.id.title);
        final TextView cardCounter = findViewById(R.id.cardCounter);

        generateCardDeck generateDeck = new generateCardDeck(questionArray, virusArray, gameArray);
        List<String> cardTitleList = generateDeck.getCardTitleList();
        List<String> cardTextList = generateDeck.getCardTextList();

//        Log.d("cardDeck", "Title size: " + cardTitleList.size() + " Text size: " + cardTextList.size());

        final int[] count = {0};
        int maxTurns = generateDeck.getMaxTurns();
        count[0] = -1;
        touch.setOnTouchListener(new OnSwipeTouchListener(PicohanActivity.this) {

            @SuppressLint("SetTextI18n")
            public void onTouch() { // go to the next card on a tap
                if (count[0] < cardTextList.size() - 1) {

                    count[0] = count[0] + 1;

                    String title = cardTitleList.get(count[0]);
                    String text = cardTextList.get(count[0]);

                    questionTitle.setText(title);
                    questionText.setText(text);
                    if (!Objects.equals(title, "Game Over")) { // This is for debugging only
                        cardCounter.setText(count[0] + 1 + "/" + maxTurns);
                    } else {
                        cardCounter.setText("");
                    }
                    setActivityBackgroundColor(retrieveColor(title));
                } else {
                    switchActivities();
                }
            }

            @SuppressLint("SetTextI18n")
            public void onSwipeLeft() { // go to the next card on a swipe
                if (count[0] < cardTextList.size() - 1) {

                    count[0] = count[0] + 1;

                    String title = cardTitleList.get(count[0]);
                    String text = cardTextList.get(count[0]);

                    questionTitle.setText(title);
                    questionText.setText(text);
                    if (!Objects.equals(title, "Game Over")) { // This is for debugging only
                        cardCounter.setText(count[0] + 1 + "/" + maxTurns);
                    } else {
                        cardCounter.setText("");
                    }
                    setActivityBackgroundColor(retrieveColor(title));
                } else {
                    switchActivities();
                }
            }

            @SuppressLint("SetTextI18n")
            public void onSwipeRight() { // go back one card on a swipe
                if (count[0] > 0) {

                    count[0] = count[0] - 1;

                    String title = cardTitleList.get(count[0]);
                    String text = cardTextList.get(count[0]);

                    questionTitle.setText(title);
                    questionText.setText(text);
                    if (!Objects.equals(title, "Game Over")) { // This is for debugging only
                        cardCounter.setText(count[0] + 1 + "/" + maxTurns);
                    } else {
                        cardCounter.setText("");
                    }
                    setActivityBackgroundColor(retrieveColor(title));
                }
            }

        });



    }

    // method to choose correct background color
    public String retrieveColor(String title) {

        String hexColor;

        if (Objects.equals(title, "Virus")) { // orange
            hexColor = "#AE5B34"; // E17D4F
        } else if (Objects.equals(title, "Cure")) { // purple
            hexColor = "#7A3170"; // 643A71
        } else if (Objects.equals(title, "Game")) { // green
            hexColor = "#4B8544"; // 66B55D
        } else if (Objects.equals(title, "Bottoms up")) { // red
            hexColor = "#A43A43"; // DD5050
        } else if (Objects.equals(title, "Game Over")) { // grey
            hexColor = "#392B3D"; // 392B3D
        } else { // default card -> blue
            hexColor = "#37356E"; // 8B5FBF
        }

        return hexColor;
    }

    // method to set background color
    public void setActivityBackgroundColor(String hexColor) {
        CardView main = findViewById(R.id.card);
        main.setCardBackgroundColor(Color.parseColor(hexColor));
    }

    // method to switch Activity
    private void switchActivities() {
        Intent switchActivityIntent = new Intent(this, MainActivity.class);
        startActivity(switchActivityIntent);
    }

}
