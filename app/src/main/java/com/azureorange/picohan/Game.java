package com.azureorange.picohan;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Game {

    public final String question;
    final List<String> playerArray;
    public static int playerCount;


    public Game(String tempQuestion) {
        super();

        // get Players and Player count
        playerArray = new LinkedList<>(PlayerSettings.getPlayerList());
        playerCount = PlayerSettings.getPlayerCount();

        Collections.shuffle(playerArray);

        tempQuestion = replacePlayerTag(tempQuestion, "\\[PRIMARY]");

        tempQuestion = replacePlayerTag(tempQuestion, "\\[SECONDARY]");

        question = tempQuestion;
    }

    public String getGame() {
        return question;
    }

    public String replacePlayerTag(String searchString, String regex) {

//        playerCount = PlayerSettings.getPlayerCount();

        Pattern regexPattern = Pattern.compile(regex);
        Matcher regexMatcher = regexPattern.matcher(searchString);

        if (regexMatcher.find()) {
            int index = new Random().nextInt(playerCount);
            searchString = searchString.replace(regexMatcher.group(), playerArray.get(index));
            playerArray.remove(index);
            playerCount--;
        }

        return searchString;
    }

}
