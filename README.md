# Picohan

Picohan will be an opensource implementation of the well known drinking game Picolo.

## build

Picohan is built using Android Studio and the OpenJDK opensource Java Platform implementation:

[java-19-openjdk](https://openjdk.org/install/)

```
./gradlew build
```
